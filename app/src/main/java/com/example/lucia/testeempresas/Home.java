package com.example.lucia.testeempresas;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lucia.testeempresas.Adapters.EnterpriseAdapter;
import com.example.lucia.testeempresas.ApiClass.ServiceLogin;
import com.example.lucia.testeempresas.HelpClass.AllEnterprises;
import com.example.lucia.testeempresas.Objects.Enterprise;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Home extends AppCompatActivity {

    private Intent intent;
    private ListView listView;
    private ArrayList<Enterprise> enterprisesArray;
    private Intent enterpriseActivity;
    private ServiceLogin serviceLogin;
    private TextView textView;
    Bundle dadosBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.darkPink));
        }

        intent = getIntent();
        serviceLogin = new Retrofit.Builder().baseUrl("http://54.94.179.135:8090/api/v1/").addConverterFactory(GsonConverterFactory.create()).build().create(ServiceLogin.class);
        listView = (ListView) findViewById(R.layout.list_view);
       requestEnterprise(intent.getStringExtra("acess-token"), intent.getStringExtra("client"),intent.getStringExtra("uid"));
    }

    //Recupera e busca a empresa no servidor
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_home,menu);
        SearchView searchView;
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem item = menu.findItem(R.id.action_searchable_activity);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            searchView = (SearchView) item.getActionView();
        }else{
            searchView = (SearchView) MenuItemCompat.getActionView(item);
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<Enterprise> enterprisesList = new ArrayList<Enterprise>();
                for(Enterprise actualEnterprise : enterprisesList){
                    if(actualEnterprise.getEnterpriseName().toLowerCase().contains(newText.toLowerCase())){
                        if(!newText.isEmpty()){
                            enterprisesList.add(actualEnterprise);
                        }
                    }
                }
                EnterpriseAdapter enterpriseAdapter  = new EnterpriseAdapter(enterprisesList,getBaseContext());
                listView.setAdapter(enterpriseAdapter);
                return true;
            }
        });
    return super.onCreateOptionsMenu(menu);
    }

    /*
    *Funcao que busco um objeto em JSON contendo os dados da empresa e converte em EnterpriseObjects
     */
    private void searchEnterprise(String input){
        Enterprise enterprise = null;
        try{
            JSONObject jsonObject = new JSONObject(input);
            JSONArray jsonArray = new JSONArray("enterprises");
            enterprisesArray = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                enterprisesArray.add(new Enterprise(Integer.parseInt(jsonObject1.optString("id")),jsonObject1.optString("enterprise_name"),
                        jsonObject1.optString("enterprise_type_name"),jsonObject1.optString("country"),jsonObject1.optString("description"),
                        jsonObject1.optString("photo")));
            }

            EnterpriseAdapter enterpriseAdapter = new EnterpriseAdapter(enterprisesArray,this);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    enterpriseActivity = new Intent(Home.this,EnterpriseActivity.class);
                    Enterprise enterprise1 = (Enterprise) listView.getItemAtPosition(position);
                    String idEnter = ""+enterprise1.getId();
                    enterpriseActivity.putExtra("id",idEnter);
                    enterpriseActivity.putExtra("enterName",enterprise1.getEnterpriseName());
                    enterpriseActivity.putExtra("enterDescr",enterprise1.getDescription());
                    enterpriseActivity.putExtra("photo",enterprise1.getPhoto());
                    startActivity(enterpriseActivity);
                }
            });

        }catch (JSONException e){
            Log.d("ERROR",e.getMessage());
        }
    }

    /*
    *Busca no servidor com os headers de acesso os dados de empresas
     */
    private void requestEnterprise(String acessToken,String client, String uid){
        serviceLogin.grantAcess(acessToken,client,uid).enqueue(new Callback<AllEnterprises>() {
            @Override
            public void onResponse(Call<AllEnterprises> call, Response<AllEnterprises> response) {
               searchEnterprise(response.body().toString());
            }

            @Override
            public void onFailure(Call<AllEnterprises> call, Throwable t) {
            Log.d("ERRO",t.getMessage());
            }
        });
    }

}
