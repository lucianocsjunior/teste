package com.example.lucia.testeempresas.Objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Classe contendo o login de acesso
 * @author lucia
 *@version 1.0
 */

public class acessToken implements Serializable {
    @SerializedName("access-token")
    private String token;

    @SerializedName("client")
    private String client;

    @SerializedName("uid")
    private String uid;

    public acessToken(){
        this.token = "";
        this.client = "";
        this.uid = "";
    }

    public acessToken(String token, String client, String uid){
        this.token = token;
        this.client = client;
        this.uid = uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "Header{"+"accessToken='"+token+'\''+", client'"+client+'\''+", userId='" + uid + '\'' + '}';
    }
}
