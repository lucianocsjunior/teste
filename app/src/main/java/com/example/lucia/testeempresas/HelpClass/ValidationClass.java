package com.example.lucia.testeempresas.HelpClass;

import android.widget.EditText;

import java.util.regex.Pattern;

/**
 * Classe que checa se o campo login e valido
 *@author lucia
 * @version 1.0
 */

public class ValidationClass {
    private static final String EMAIL_BASE = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String MSG = "Campo Requirido ou Tamanho Inválido";
    private static final String MSG_INVALID_FIELD = "Campo Vazio";
    private static final String ERROR_MSG = "Email Inválido";

    /*
    *Metodo que checa se o campo tem o tamanho valido
    * @param EditText editText contendo a string inserida no campo
     *@return true caso o campo valido
     */
    public static boolean isValidField(EditText editText){
        String text = editText.getText().toString().trim();
        editText.setError(null);
        if(text.length()<3){
            editText.setError(MSG_INVALID_FIELD);
            return false;
        }
        return true;
    }

    /*
    *Metodo que checa se o campo estar vazio
    * @param EditText editText contendo a string inserida no campo
     *@return true caso o campo valido
     */

    public static boolean hasText(EditText editText){
        String text = editText.getText().toString().trim();
        editText.setError(null);
        if(text.length() == 0){
            editText.setError(MSG);
            return false;
        }
        return true;
    }

    /*
    *Metodo que checa se o campo e valido
    * @param EditText editText contendo a string inserida no campo
     *@return true caso o campo valido
     */

    public static boolean isValid(EditText editText,String str1,String errMsg, boolean required){
        String text = editText.getText().toString().trim();
        editText.setError(null);
        if(required && !hasText(editText) && !isValidField(editText))return false;
        if(required && !Pattern.matches(str1,text)){
            editText.setError(errMsg);
            return false;
        };
        return true;
    }

    public static boolean isEmailAdress(EditText editText,boolean required){
        return  isValid(editText,EMAIL_BASE,ERROR_MSG,required);
    }
}
