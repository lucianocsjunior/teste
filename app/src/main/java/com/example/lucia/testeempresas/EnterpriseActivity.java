package com.example.lucia.testeempresas;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class EnterpriseActivity extends AppCompatActivity {
    private TextView enterpriseName;
    private ImageView enterpriseLogo;
    private TextView enterpriseDescription;
    private ImageButton imageButton;
    private Toolbar toolbar;
    private Bundle bundle;
    private RelativeLayout relativeLayout;
    private RelativeLayout relativeLayout1;
    private TextView enterpriseNoLogo;
    private String enterpriseNoLogoS;
    private Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enterprise);
        imageButton = (ImageButton) findViewById(R.id.imageButton);
        enterpriseName = (TextView)findViewById(R.id.enterpriseOnlyAct);
        enterpriseLogo = (ImageView)findViewById(R.id.imageView);
        enterpriseDescription = (TextView)findViewById(R.id.descriptionEnter);
        enterpriseNoLogo = (TextView) findViewById(R.id.enterDesc);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativelayout2E);
        relativeLayout1 = (RelativeLayout)findViewById(R.id.relativelayout1E);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        intent = getIntent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.darkPink));
        }

        //Caso a empresa nao contenha foto, recupera o id e seta como E+id
        if(intent.getStringExtra("photo") == null || intent.getStringExtra("photo").equals("")){
            enterpriseLogo.setVisibility(View.INVISIBLE);
            relativeLayout.setBackgroundColor(R.color.buttonColor);
             enterpriseNoLogoS = "E" + intent.getStringExtra("id");
            enterpriseNoLogo.setText(enterpriseNoLogoS);
        }else{
            relativeLayout.setVisibility(View.INVISIBLE);
            enterpriseNoLogo.setText("");
            Glide.with(this).load(intent.getStringExtra("photo")).override(318,155).into(enterpriseLogo);
        }

        enterpriseName.setText(intent.getStringExtra("enterName"));
        enterpriseDescription.setText(intent.getStringExtra("enterDescr"));

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(EnterpriseActivity.this,Home.class);
                startActivity(intent1);
            }
        });

    }
}
