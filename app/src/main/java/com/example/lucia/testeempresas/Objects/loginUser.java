package com.example.lucia.testeempresas.Objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Classe para recuperar o login
 * @author lucia
 * @version 1.0
 */

public class loginUser implements Serializable {

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    public loginUser(){
        this.username = " ";
        this.password = " ";

    }

    public loginUser(String username,String password){
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
