package com.example.lucia.testeempresas.HelpClass;

import com.example.lucia.testeempresas.Objects.Enterprise;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Classe responsavel por retornar as listas de empresas
 * @author lucia
 * @version 1.0
 */

public class AllEnterprises implements Serializable {
    @SerializedName("enterprises")
    @Expose
    private List<Enterprise> enterprises;

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Enterprise> enterprises) {
        this.enterprises = enterprises;
    }

    @Override
    public String toString() {
        String enterpriseName = "";
        for(Enterprise enterprise:enterprises){
            enterpriseName += enterprise.getEnterpriseName();
        }

        return enterpriseName;
    }
}
