package com.example.lucia.testeempresas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lucia.testeempresas.ApiClass.ServiceLogin;
import com.example.lucia.testeempresas.HelpClass.ValidationClass;
import com.example.lucia.testeempresas.Objects.acessToken;
import com.example.lucia.testeempresas.Objects.loginUser;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private EditText userLogin;
    private EditText userPass;
    private Button loginButton;
    private loginUser login;
    private acessToken acessToken1;
    public Retrofit retrofit = new Retrofit.Builder().baseUrl("http://54.94.179.135:8090/api/v1/").addConverterFactory(GsonConverterFactory.create()).build();
    Toast toast;
    int duration = Toast.LENGTH_LONG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerFields();
    }

   private boolean checkCredentials(){
       boolean ret = true;
       if(!ValidationClass.hasText(userLogin) && !ValidationClass.isValidField(userLogin)) ret = false;
       if(!ValidationClass.isEmailAdress(userLogin,true)) ret = false;
        return ret;
   }

   private void registerFields(){
       userLogin = (EditText) findViewById(R.id.editText4);
       userLogin.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {}

           @Override
           public void afterTextChanged(Editable s) {
                ValidationClass.hasText(userLogin);
               ValidationClass.isValidField(userLogin);
           }
       });

       userPass = (EditText) findViewById(R.id.editText5);
       userPass.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {}

           @Override
           public void afterTextChanged(Editable s) {
                ValidationClass.hasText(userPass);
               ValidationClass.isValidField(userPass);
           }
       });

       loginButton = (Button) findViewById(R.id.button);
       loginButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(MainActivity.this,Home.class);
             if (checkCredentials()) {
                    retrofit.create(ServiceLogin.class).userLogin(userLogin.getText().toString(),userPass.getText().toString()).enqueue(new Callback<loginUser>() {
                        @Override
                        public void onResponse(Call<loginUser> call, Response<loginUser> response) {
                            if(response.isSuccessful()){
                                Intent intent = new Intent(MainActivity.this,Home.class);
                                intent.putExtra("acess-token",response.headers().get("access-token"));
                                intent.putExtra("client",response.headers().get("client"));
                                intent.putExtra("uid",response.headers().get("uid"));
                                Log.d("Header",response.headers().get("acess-token"));
                                toast = Toast.makeText(getApplicationContext(),"Successful",duration);
                                toast.show();
                                startActivity(intent);
                            }else{
                                toast = Toast.makeText(getApplicationContext(),response.message(),duration);
                                toast.show();
                                Log.d("Error",response.message());
                            }
                        }
                        @Override
                        public void onFailure(Call<loginUser> call, Throwable t) {

                        }
                    });
               }

           }
       });
   }
}
