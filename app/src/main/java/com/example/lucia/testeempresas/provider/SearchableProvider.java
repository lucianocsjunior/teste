package com.example.lucia.testeempresas.provider;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by lucia on 27/07/2017.
 */

public class SearchableProvider extends SearchRecentSuggestionsProvider {
    public static final String AUTHORITY = "br.com.example.lucia.testeempresas.provider.SearchableProvider";
    public static final int MODE = DATABASE_MODE_QUERIES;

    public SearchableProvider(){
        setupSuggestions( AUTHORITY, MODE );
    }


}
