package com.example.lucia.testeempresas.Objects;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Classe para recuperar os dados do Objeto empresas
 * @author lucia
 * @version 1.0
 */

public class Enterprise implements Serializable {
    @SerializedName("id")
    private int id;

    @SerializedName("enterpriseName")
    private String enterpriseName;

    @SerializedName("business")
    private String business;

    @SerializedName("country")
    private String country;

    @SerializedName("description")
    private String description;

    @SerializedName("photo")
    private String photo;

    public  Enterprise(){
        this.id = id;
        this.enterpriseName = enterpriseName;
        this.business = business;
        this.country = country;
        this.description = description;
        this.photo = photo;
    }

    public Enterprise(int id, String enterpriseName,String business, String country, String description, String photo ){
        this.id = id;
        this.enterpriseName = enterpriseName;
        this.business = business;
        this.country = country;
        this.description = description;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
